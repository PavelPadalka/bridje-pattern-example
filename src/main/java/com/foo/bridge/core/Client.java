package com.foo.bridge.core;

import com.foo.bridge.engine.BigEngine;
import com.foo.bridge.engine.SmallEngine;
import com.foo.bridge.vehicle.BigBus;
import com.foo.bridge.vehicle.SmallCar;
import com.foo.bridge.vehicle.Vehicle;

public class Client {

    public static void main(String[] args) {

        Vehicle bigBus = new BigBus(new SmallEngine(100));
        bigBus.drive();
        bigBus.setEngine(new BigEngine(600));
        bigBus.drive();

        System.out.println("\n");

        Vehicle smallCar = new SmallCar(new SmallEngine(200));
        smallCar.drive();
        smallCar.setEngine(new BigEngine(700));
        smallCar.drive();

    }

}
