package com.foo.bridge.engine;

public class BigEngine implements IEngine {

    private int power;

    public BigEngine(int power) {
        this.power = power;
    }

    @Override
    public int go() {
        System.out.println("Big engine is running");
        return power;
    }
}
