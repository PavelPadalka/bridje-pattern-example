package com.foo.bridge.engine;

public interface IEngine {

    int go();

}
