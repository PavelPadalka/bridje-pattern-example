package com.foo.bridge.engine;

public class SmallEngine implements IEngine {

    private int power;

    public SmallEngine(int power) {
        this.power = power;
    }

    @Override
    public int go() {
        System.out.println("Small engine is running");
        return power;
    }
}
