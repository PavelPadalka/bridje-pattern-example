package com.foo.bridge.vehicle;

import com.foo.bridge.engine.IEngine;

public class BigBus extends Vehicle {

    public BigBus(IEngine engine) {
        super.engine      = engine;
        super.weightKilos = 3000;
    }

    @Override
    public void drive() {
        System.out.println("Big bus is driving");
        int power = engine.go();
        reportOnSpeed(power);
    }
}
