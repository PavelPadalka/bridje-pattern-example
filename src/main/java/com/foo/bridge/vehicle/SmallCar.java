package com.foo.bridge.vehicle;

import com.foo.bridge.engine.IEngine;

public class SmallCar extends Vehicle{

    public SmallCar(IEngine engine) {
        super.engine      = engine;
        super.weightKilos = 600;
    }

    @Override
    public void drive() {
        System.out.println("Small car is driving");
        int power = engine.go();
        reportOnSpeed(power);
    }
}
