package com.foo.bridge.vehicle;

import com.foo.bridge.engine.IEngine;

public abstract class Vehicle {

    public IEngine engine;
    public int weightKilos;

    public abstract void drive();

    public void setEngine(IEngine engine) {
        this.engine = engine;
    }

    public void reportOnSpeed(int power) {

        int ratio = weightKilos / power;

        if (ratio < 3 ){
            System.out.println("Fast speed");
        }else if(ratio >= 3 && ratio < 8) {
            System.out.println("Average speed");
        }else {
            System.out.println("Slow speed");
        }

    }

}
